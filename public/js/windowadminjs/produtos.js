var ispudate = false;
var tavbleaniver = $('#example').DataTable({

    "oLanguage": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Exibindo de _START_ até _END_ no total de _TOTAL_ registros.",
        "sInfoEmpty": "",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sSearch": "Pesquisar: ",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        }
    }, "fnDrawCallback": function () {


    }, columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: "<a href='#' id='cc'><i class='material-icons blue-grey-text'>create</i></a> <a class='deletepro' href='#' data-target='deleteprodu' id='bb'><i class='material-icons red-text'>delete</i></a>"
    }],

    order: [[1, 'asc']]

});
$('.modal').modal();


$('#slidernewporo').click(() => {
    $('#ocuta').removeAttr('checked');
    $('#abilitarpromo').removeAttr('checked');
    ispudate = false
});


tavbleaniver.on('search.dt', function () {
    console.log('Currently applied global search: ' + tavbleaniver.search());
});

$('#novopro').sidenav({
    edge: 'right', onOpenStart: () => {


        if (ispudate) {
            $('#cadasprodut').val('Atualizar');
            $('#cadasprodut').removeClass("indigo");
            $('#cadasprodut').addClass("blue");
            $('#titlesidebarprodu').html('Atualizar Produto')
        } else {

            $("#produtoname").html("Novo Produto");
            $("#serial").html("");


            $('#cadasprodut').val('Cadastrar');
            $('#cadasprodut').removeClass("blue");
            $('#cadasprodut').addClass("indigo");
            $('#titlesidebarprodu').html('Novo Produto')
        }

    }
});


var wit = $('.sidenavcuston').css('width');
var newwidth = (parseInt(wit) - 300) + 30;
$('.sidenavcuston').css('width', newwidth);

$('#recusarproduto').click(() => {

    ispudate = false;
    $('#novopro').sidenav('close');

});


$('#porcen').attr("disabled", "");
$('#abilitarpromo').change(function (e) {

    const check = e.target.checked;

    if (check) {
        $('#porcen').removeAttr("disabled")
    } else {
        $('#porcen').attr("disabled", "")
    }


});


getCategorias();
getProdutos();

function getCategorias() {


    var data = {method: "gecategorias"};

    Rx.DOM.ajax({
        url: '/', method: 'POST', body: data, headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).subscribe(
        function (next) {

            var dados = JSON.parse(next.response);


            for (var i = 0; i < dados.length; i++) {

                var value = dados[i];

                $('#cate').append(new Option(value.nome, value.id))


            }


        },

        function (error) {

            console.error(error)
        },

        function () {

            $("select").formSelect();


            $('#formprodu').submit(function (e) {

                e.preventDefault();

                if (ispudate) {

                    update()

                } else {
                    insertProduto();
                }


            });

            /*  $('#formprodu').reset(function (e) {

                  e.preventDefault();

                  $('#novopro').sidenav('close');


              }); */

        }
    )


}

function getProdutos() {

    var valores;
    var data = {method: "getProduts"};

    Rx.DOM.ajax({
        url: '/', method: 'POST', body: data, headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).map((value) => {


        return value;

    })
        .subscribe(
            function (next) {

                var dados = JSON.parse(next.response);

                valores = dados;
                for (var i = 0; i < dados.length; i++) {

                    var value = dados[i];


                    tavbleaniver.row.add([value.produto.id, value.produto.nome, porce(value.produto.abilitpro, value.produto.valorpro, value.produto.precov), value.produto.quanti, value.cate.nome]).draw();


                }


            },

            function (error) {

                console.error(error)
            },

            function () {

                $("select").formSelect();

                $('#example tbody tr ').on('click', 'a#bb', function (e) {

                    var data = tavbleaniver.row($(this).parents('tr')).data()[0];


                    valores.forEach((valor) => {

                        if (valor.produto.id === data) {

                            $("#deleteprodu").modal('open');
                            exclurproduto(data)

                        }

                    })
                });

                $('#example tbody ').on('click', 'tr', function (e) {


                    var data = tavbleaniver.row(this).data()[0];

                    valores.forEach((valor) => {

                        if (valor.produto.id === data) {

                            $('html,body').animate({
                                scrollTop: $("#detalhesprodu").offset().top - 60
                            }, 'slow');

                            exebir(valor.produto)
                        }

                    })


                });


                $('#example tbody tr ').on('click', 'a#cc', function (e) {


                    var data = tavbleaniver.row($(this).parents('tr')).data()[0];

                    valores.forEach((valor) => {

                        if (valor.produto.id === data) {

                            ispudate = true;
                            setvalues(valor.produto)

                        }

                    })
                });


            }
        )


}

function insertProduto() {


    if ($('#nomep').val() === "") {

        M.toast({html: "Insira nome do produto"});

        return
    }

    if ($('#descricao').val() === "") {

        M.toast({html: "Insira a descrição do produto"});

        return
    }

    if ($('#precoc').val() === "") {

        M.toast({html: "Insira  preço de custo do produto"});

        return
    }

    if ($('#precov').val() === "") {

        M.toast({html: "Insira preço de venda do produto"});

        return
    }


    if ($('#cate').val() === '00') {


        M.toast({html: "Escolha a categoria do produto"});

        return
    }

    if ($('#medida').val() === "00") {


        M.toast({html: "Escolha o tipo de medida"});

        return
    }

    if ($('#vmedida').val() === "") {

        M.toast({html: "Insira o valor de medida"});

        return
    }

    if ($('#quantida').val() === "") {

        M.toast({html: "Insira quantidade total do produto no estoque"});

        return
    }

    if ($('#imgpri').val() === "") {

        M.toast({html: "Insira o nome da imagem"});

        return
    }

    if ($('#abilitarpromo').is(':checked')) {

        if ($('#porcen').val() === "") {

            M.toast({html: "Insira o valor em porcetagem (%)"});

            return
        }

    }


    var data = {
        nome: $("#nomep").val(),
        drecrisao: $('#descricao').val(),
        precoc: $('#precoc').val(),
        precov: $('#precov').val(),
        categoria: $('#cate').val(),
        medida: $('#medida').val(),
        vmedida: $('#vmedida').val(),
        quatidade: $('#quantida').val(),
        ocuta: $('#ocuta').is(':checked'),
        abilitapro: $('#abilitarpromo').is(':checked'),
        valorpromo: $('#porcen').val(),
        fotoname1: $('#imgpri').val(),
        fotoname2: $('#imgpri1').val(),
        fotoname3: $('#imgpri2').val(),
        fotoname4: $('#imgpri3').val(),
        method: 'insertProdut'
    };



    Rx.DOM.ajax({
        url: '/', method: 'POST', body: data, headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

    }).subscribe(function (next) {

        var js = JSON.parse(next.response);

        if (js.status === 'ok') {

            M.toast({html: "Atualizado com sucesso"});
            $('#novopro').sidenav('close');

        }

    }, function (err) {
        console.error(err)
    }, function () {

        reloadPage("#2")

    })

}


function setvalues(dat) {


    $("#cate").val(dat.idcategoria);
    $("#medida").val(dat.tipomedida);

    $("#cate").formSelect();
    $("#medida").formSelect();


    $("#produtoname").html(dat.nome);
    $("#serial").html(dat.id);


    $("#nomep").val(dat.nome);
    $('#nomepl').addClass('active');

    $('#descricao').val(dat.descricao);
    $('#descricaol').addClass('active');

    $('#precoc').val(dat.precoc);
    $('#precocl').addClass('active');

    $('#precov').val(dat.precov);
    $('#precovl').addClass('active');


    // $('#cate').addClass('active');


    // $('#nomepl').addClass('active');

    $('#vmedida').val(dat.valormedida);
    $('#vmedidal').addClass('active');

    $('#quantida').val(dat.quanti);
    $('#quantidal').addClass('active');

    if (dat.ocutar === true) {
        $('#ocuta').attr('checked', '');
    } else {
        $('#ocuta').removeAttr('checked');

    }


    if (dat.abilitpro === true) {
        $('#abilitarpromo').attr('checked', '');

        $('#porcen').val(dat.valorpro);
        $('#porcenl').addClass('active');

    } else {

        $('#abilitarpromo').removeAttr('checked');
    }

    $('#imgpri').val(dat.fotoname1);
    $('#imgpril').addClass('active');


    if (dat.fotoname2 !== "") {

        $('#imgpri1').val(dat.fotoname2);
        $('#imgpri1l').addClass('active');
    }

    if (dat.fotoname3 !== "") {
        $('#imgpri2').val(dat.fotoname3);
        $('#imgpri2l').addClass('active');
    }


    if (dat.fotoname4 !== "") {
        $('#imgpri3').val(dat.fotoname4);
        $('#imgpri3l').addClass('active');
    }


    $('#novopro').sidenav('open');

}


function update(id) {


    if ($('#nomep').val() === "") {

        M.toast({html: "Insira nome do produto"});

        return
    }

    if ($('#descricao').val() === "") {

        M.toast({html: "Insira a descrição do produto"});

        return
    }

    if ($('#precoc').val() === "") {

        M.toast({html: "Insira  preço de custo do produto"});

        return
    }

    if ($('#precov').val() === "") {

        M.toast({html: "Insira preço de venda do produto"});

        return
    }


    if ($('#cate').val() === '00') {


        M.toast({html: "Escolha a categoria do produto"});

        return
    }

    if ($('#medida').val() === "00") {


        M.toast({html: "Escolha o tipo de medida"});

        return
    }

    if ($('#vmedida').val() === "") {

        M.toast({html: "Insira o valor de medida"});

        return
    }

    if ($('#quantida').val() === "") {

        M.toast({html: "Insira quantidade total do produto no estoque"});

        return
    }

    if ($('#imgpri').val() === "") {

        M.toast({html: "Insira o nome da imagem"});

        return
    }

    if ($('#abilitarpromo').is(':checked')) {

        if ($('#porcen').val() === "") {

            M.toast({html: "Insira o valor em porcetagem (%)"});

            return
        }

    }


    var data = {
        nome: $("#nomep").val(),
        drecrisao: $('#descricao').val(),
        precoc: $('#precoc').val(),
        precov: $('#precov').val(),
        categoria: $('#cate').val(),
        medida: $('#medida').val(),
        vmedida: $('#vmedida').val(),
        quatidade: $('#quantida').val(),
        ocuta: $('#ocuta').is(':checked'),
        abilitapro: $('#abilitarpromo').is(':checked'),
        valorpromo: $('#porcen').val(),
        fotoname1: $('#imgpri').val(),
        fotoname2: $('#imgpri1').val(),
        fotoname3: $('#imgpri2').val(),
        fotoname4: $('#imgpri3').val(),
        method: 'upProduts',
        id: $("#serial").html()
    };

    Rx.DOM.ajax({
        url: '/', method: 'POST', body:data, headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

    }).subscribe(function (next) {

        var js = JSON.parse(next.response);

        if (js.status === 'ok') {

            M.toast({html: "Atualizado com sucesso"});
            $('#novopro').sidenav('close');

        }

    }, function (err) {
        console.error(err)
    }, function () {

        reloadPage("#2")

    })

}


function exclurproduto(dataa, value) {


    $("#prosim").unbind('click').click(() => {


        var data = {method: "deleteProduts", id: dataa};

        Rx.DOM.ajax({
            url: '/', method: 'POST', body: data, headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).subscribe(function (next) {

            var js = JSON.parse(next.response);
            if (js.status === 'ok') {

                M.toast({html: "Atualizado com sucesso"});

                //  drop.dropdown('destroy');

                reloadPage("#2")
            }


        }, function (err) {
            console.error(err)
        }, function () {

            $("#deleteprodu").modal('close');
        })
    });
}


function porce(isbool, porv, value) {


    if (isbool) {

        var val = (value * (porv / 100));


        return '<label class="red-text">R$' + value + '</label>/<label class="green-text">R$ ' + (value - val) + ' </label>'


    } else {

        return '<label class="green-text">R$' + value + '</label>'

    }

}


function exebir(data) {






    numeral.locale('pt-br');

    var images = $('#imagess');


    $('#nome-cap').html(data.nome);
    $('#descri').html(data.descricao);
    $('#precovenda').html(numeral(data.precov).format('$00.00'));
    $('#precocompra').html(numeral(data.precoc).format('$00.00'));
    $('#quantidade').html(data.quanti + " Unidades");


    $('#tmedida').html(mediddas(data.tipomedida));
    $('#vamedida').html(data.valormedida);
    $('#abilite').html(abilitarPromocao(data.abilitpro));

    $('#valorpro').html(data.valorpro);


    $('#ocutarprodu').html((data.ocutar ? "abilitado" : "Não abilitado"));

    images.html("<a class='carousel-item'><img  style=' border-radius: 20px; border: 1px solid transparent ; '  height='400' src='./images?id=" + data.fotoname1 + "'/></a>");

    if (data.fotoname2 !== "") {
        images.append("<a class='carousel-item'><img style=' border-radius: 20px; border: 1px solid transparent ;'  height='400' src='./images?id=" + data.fotoname2 + "'/></a>");

    }

    if (data.fotoname3 !== "") {
        images.append("<a class='carousel-item'><img style=' border-radius: 20px; border: 1px solid transparent ;'  height='400' src='./images?id=" + data.fotoname3 + "'/></a>");
    }

    if (data.fotoname4 !== "") {
        images.append("<a class='carousel-item'><img style=' border-radius: 20px; border: 1px solid transparent ;'  height='400' src='./images?id=" + data.fotoname4 + "'/></a>");
    }

    $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true
    });
}


function abilitarPromocao(i) {

    var m = null;

    switch (i) {

        case  true :
            m = "<p class='green-text'> Abilitado</p>";
            break;
        case  false :
            m = "<p class='red-text'> Não abilitado</p>";
            break;

    }


    return m;
}


function mediddas(i) {

    var m = null;

    switch (i) {

        case  1 :
            m = "Kilo(Kg)";
            break;
        case  2 :
            m = "Grama(g)";
            break;
        case  3 :
            m = "Litros(L)";
            break;
        case  4 :
            m = "Mililitros(ml)";
            break;
        case  5 :
            m = "Metro(m)";
            break;
        case  6 :
            m = "Centimetro(cm)";
            break;
    }

    return m;
}
