$('#categoriaconteiner').modal();

var drop = null;


var tavble = $('#tablecate').DataTable({

    "oLanguage": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Exibindo de _START_ até _END_ no total de _TOTAL_ registros.",
        "sInfoEmpty": "",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sSearch": "Pesquisar: ",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        }
    }, "fnDrawCallback": function () {


    }, columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: "<a href='#' id='cc'><i class='material-icons blue-grey-text'>create</i></a> <a class='caixa ' href='#' data-target='deletecate' id='bb'><i class='material-icons red-text'>delete</i></a>"
    }],

    order: [[1, 'asc']]


});


getCategorias();
insertcate();


function getCategorias() {


    var data = {method: "gecategorias"};

    Rx.DOM.ajax({
        url: '/', method: 'POST', body: data, headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).subscribe(
        function (next) {

            var dados = JSON.parse(next.response);

            tavble.clear().draw();

            for (var i = 0; i < dados.length; i++) {

                var value = dados[i];


                tavble.row.add([value.id, value.nome, value.itcadas, wsith(value.id, value.ativado)]).draw();


            }


        },

        function (error) {

            console.error(error)
        },

        function () {


            $("select").formSelect();


            $('#tablecate tbody').on('click', 'a#cc', function () {
                var data = tavble.row($(this).parents('tr')).data();

            });


            $('#tablecate tbody tr').on('click', 'a#bb', function (e) {

                var i;

                var data = tavble.row($(this).parents('tr'));
                i = data.data()[0];

                var observeble = Rx.Observable.create((ob) => {


                    ob.next(i)

                });

                observeble.subscribe((next) => {

                    exclurCategorias(next, e.target);


                }, (err) => {
                }, () => {


                });


            });


            drop = $(".caixa");
            drop.dropdown({hover: false, constrainWidth: false,});


            upAtivado();

        }
    )


}


function exclurCategorias(dataa, value) {


    $("#catesim").unbind('click').click(() => {


        var data = {method: "excluircate", id: dataa};

        Rx.DOM.ajax({
            url: '/', method: 'POST', body: data, headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).subscribe(function (next) {

            var js = JSON.parse(next.response);
            if (js.status === 'ok') {

                M.toast({html: "Atualizado com sucesso"});

                //  drop.dropdown('destroy');

                reloadPage("#3")
            }


        }, function (err) {
            console.error(err)
        }, function () {


        })
    });
}

function insertcate() {


    $("#cadasnewcate").unbind('click').click(() => {

        if ($('#newcate').val() === "") {

            M.toast({html: "Insira nome da categoria"});

            return
        }

        var data = {method: "insertcategoria", nome: $('#newcate').val()};

        Rx.DOM.ajax({
            url: '/', method: 'POST', body:data, headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).subscribe(function (next) {

            var js = JSON.parse(next.response);
            if (js.status === 'ok') {

                $('#categoriaconteiner').modal('close');

                M.toast({html: "Atualizado com sucesso"});

                reloadPage("#3")
            }


        }, function (err) {
            console.error(err)
        }, function () {


        })
    });

}

function upAtivado() {

    $('.switchcate').change(function (e) {

        const check = e.target.checked;
        const value = $(this).val();

        var data = {method: "upcheckcate", check:check,id:value};

        Rx.DOM.ajax({
            url: '/', method: 'POST', body: data, headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).subscribe(function (next) {

            var js = JSON.parse(next.response);
            if (js.status === 'ok') {



                M.toast({html: "Atualizado com sucesso"});


            }


        }, function (err) {
            console.error(err)
        }, function () {


        })

    })
}


function wsith(id, value) {

    if (value) {
        return "<div class='switch'><label> <input class='switchcate' value='" + id + "' checked type='checkbox'/> <span class='lever'></span>  </label></div>"

    } else {
        return "<div class='switch'><label> <input class='switchcate' value='" + id + "' type='checkbox'/> <span class='lever'></span>  </label></div>"

    }

}

