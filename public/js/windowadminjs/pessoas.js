var tavbleaniver = $('#pessoastable').DataTable({

    "oLanguage": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Exibindo de _START_ até _END_ no total de _TOTAL_ registros.",
        "sInfoEmpty": "",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado.",
        "sSearch": "Pesquisar: ",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        }
    }, "fnDrawCallback": function () {


    }, columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
    },{
        targets: -1,
        data: null,
        defaultContent: " <a class='dropdown-trigger ' href='#' data-target='exclurpostdropdown' id='bb'><i class='material-icons red-text'>delete</i></a>"
    }],
    select: {
        style: 'multi',
        selector: 'td:first-child'
    },
    order: [[1, 'asc']]


});


$(".modal").modal();




getClientes();


function getClientes() {

    var valores;
    var data = {method: "getclinet"};

    Rx.DOM.ajax({
        url: '/', method: 'POST', body: data, headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).map((value) => {


        return value;

    })
        .subscribe(
            function (next) {

                var dados = JSON.parse(next.response);



                    valores = dados;
                for (var i = 0; i < dados.length; i++) {

                    const value = dados[i];

                    setTimeout(function () {
                        tavbleaniver.row.add([value.id, value.nome, value.email,value.telefone, ""]).draw();
                    }, 200);




                }


            },

            function (error) {

                console.error(error)
            },

            function () {

                $("select").formSelect();

                $('#pessoastable tbody tr ').on('click', 'a#bb', function (e) {

                    var data = tavbleaniver.row($(this).parents('tr')).data()[0];


                    valores.forEach((valor) => {

                        if (valor.id === data) {

                            $("#deletecli").modal('open');


                            deleteclient(data)

                        }

                    })
                });

                $('#pessoastable tbody ').on('click', 'tr', function (e) {


                    var data = tavbleaniver.row(this).data()[0];

                   valores.forEach((valor) => {

                        if (valor.id === data) {

                            $('html,body').animate({
                                scrollTop: $("#detalhesclient").offset().top - 60
                            }, 'slow');

                            exebir(valor)
                        }

                    })


                });

            }
        )


}


function deleteclient(value) {


    $("#clisim").unbind('click').click(function (e) {


        var data = {method: "deleteclinet",data:value};

        Rx.DOM.ajax({
            url: '/', method: 'POST', body: data, headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).subscribe((next)=>{

            var js = JSON.parse(next.response);
            if (js.status === 'ok') {

                M.toast({html: "Atualizado com sucesso"});



                reloadPage("#8")
            }


        },(error)=>{},()=>{


            $("#deletecli").modal('close');
        })


    });





}

function exebir(value) {


    $("#nomecli").html(value.nome);
    $("#enderecocli").html(value.endereco);
    $("#numerocli").html(value.nu);
    $("#cidadecli").html(value.cidade);
    $("#telefonecli").html(value.telefone);
    $("#emailcli").html(value.email);





}