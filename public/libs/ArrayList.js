'use strict';


class ArrayList {


    constructor() {


        this.array = [];


    }


    add(ob) {

        if (this.array.indexOf(ob) === -1) {
            this.array.push(ob);

            return true;
        } else {
            return false;
        }


    }


    contains(ob) {

        return this.array.includes(ob);

    }

    /**
     *
     * @param {JSONOB} ob
     * @return {boolean}
     */
    containsObj(ob) {

        var pp = false;

        this.array.forEach((value, index) => {


            if (Object.keys(ob)[0] === Object.keys(value)[0]) {

                pp = true;
            }


        });

        return pp;
    }

    /** @param {String} name */
    containsObjName(name) {

        var pp = false;

        this.array.forEach((value, index) => {


            if (name === Object.keys(value)[0]) {

                pp = true;
            }


        });

        return pp;
    }

    /** @param {String} key
     * @param {function(value,number)} fn*/
    forKeyObject(key,fn){

        this.array.forEach((value, index) => {

            if (key === Object.keys(value)[0]) {

                for(var int = 0 ;  int <=  value[key].length ;int++){

                    if(value[key].hasOwnProperty(int)){

                        fn(value[key][int],int)

                    }



                }


              /*  .forEach((va,n) =>{



                }); */


            }


        });

    }


    remove(ob) {

        if (this.array.length > 0) {

            if (this.array.length === 1) {
                this.array = []
            } else {

                var it = this.array.indexOf(ob);

                for (var i = it; i < (this.array.length - 1); i++) {

                    this.array[i] = this.array[i + 1];
                }
                this.array.pop();
            }

        }


    }
    /** @param {number} number  */
    removeInt(number) {

        if (this.array.length > 0) {

            if (this.array.length === 1) {
                this.array = []
            } else {


                for (var i = number; i < (this.array.length - 1); i++) {

                    this.array[i] = this.array[i + 1];
                }
                this.array.pop();
            }

        }


    }

    /** @return {[]}  */
    getAll() {

        return this.array

    }

}