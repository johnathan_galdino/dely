/**
 * Aquivo responsavel pela contrução das tabelas no banco de dados
 *
 */


const Sequelize = require('sequelize');
const Database = require('./database');


/**
 * tabelas em ORM
 */
var clientes, categoria, device, chat;
var produtos, salas, admin;


class PojoController {


    /**
     *
     * @return {PojoController}
     */
    static get getInstance() {

        this.value = new PojoController();

        if (this.value !== null) {

            return this.value
        } else {

            this.value = new PojoController();
            return this.value
        }

    }


    constructor() {

        init()


    }

    /**
     *
     * @return {Modal}
     */
    get getCategoria() {

        return categoria;
    }

    /**
     *
     * @return {Model}
     */
    get getProduto() {


        return produtos;
    }

    /**
     *
     * @return {Modal}
     */
    get getAdmin() {

        return admin;
    }

    /**
     *
     * @return {Modal}
     */
    get getDevices() {

        return device;
    }


    /**
     *
     * @return {Modal}
     */
    get getClientes() {

        return clientes;
    }


    /**
     *
     * @return {Modal}
     */
    get getSalas() {

        return salas;
    }


    /**
     *
     * @return {Modal}
     */
    get getChat() {

        return chat;
    }
}

module.exports = PojoController;


var init = () => {


    var databse = new Database();

    databse.connect();

    clientes = databse.getDatabse.define('clientes', {

        nome: {
            type: Sequelize.DataTypes.STRING
        },
        endereco: {
            type: Sequelize.DataTypes.STRING
        },
        nu: {
            type: Sequelize.DataTypes.STRING(6)
        },
        cidade: {
            type: Sequelize.DataTypes.STRING
        },
        telefone: {
            type: Sequelize.DataTypes.STRING(15)
        },
        email: {
            type: Sequelize.DataTypes.STRING
        },
        senha: {
            type: Sequelize.DataTypes.STRING(90)
        },
        acesso: {
            type: Sequelize.DataTypes.STRING(15)
        }
        , idfirebase: {
            type: Sequelize.DataTypes.STRING(90)
        }
        , typecli: {
            type: Sequelize.DataTypes.INTEGER
        }
    });

    produtos = databse.getDatabse.define("produtos", {
        nome:
            {type: Sequelize.DataTypes.STRING}
        ,
        precoc: {
            type: Sequelize.DataTypes.STRING(15)
        },
        precov: {
            type: Sequelize.DataTypes.STRING(15)
        }
        ,
        quanti: {
            type: Sequelize.DataTypes.STRING(15)
        },
        idcategoria: {
            type: Sequelize.DataTypes.INTEGER
        },
        fotoname1: {
            type: Sequelize.DataTypes.STRING(90)
        },
        fotoname2: {
            type: Sequelize.DataTypes.STRING(90)
        },
        fotoname3: {
            type: Sequelize.DataTypes.STRING(90)
        },
        fotoname4: {
            type: Sequelize.DataTypes.STRING(90)
        },
        descricao: {
            type: Sequelize.DataTypes.TEXT
        },
        tipomedida: {
            type: Sequelize.DataTypes.INTEGER
        },
        valormedida: {
            type: Sequelize.DataTypes.STRING(15)
        },
        abilitpro: {
            type: Sequelize.DataTypes.BOOLEAN
        },
        valorpro: {
            type: Sequelize.DataTypes.INTEGER
        },

        ocutar: {
            type: Sequelize.DataTypes.BOOLEAN
        },

    });

    categoria = databse.getDatabse.define("categoria", {
        nome: {
            type: Sequelize.DataTypes.STRING(90)
        }, itcadas: {
            type: Sequelize.DataTypes.INTEGER
        }, ativado: {
            type: Sequelize.DataTypes.BOOLEAN
        }
    });

    device = databse.getDatabse.define("device", {
        token: {
            type: Sequelize.DataTypes.STRING
        },
        idclientes: {
            type: Sequelize.DataTypes.INTEGER
        }
    });

    salas = databse.getDatabse.define("salas", {
        idclient: {
            type: Sequelize.DataTypes.INTEGER
        }, endereco: {
            type: Sequelize.DataTypes.STRING
        }, naol: {
            type: Sequelize.DataTypes.INTEGER
        }
    });


    admin = databse.getDatabse.define("admins", {
        type: {
            type: Sequelize.DataTypes.INTEGER
        },
        nome: {
            type: Sequelize.DataTypes.STRING
        },
        email: {
            type: Sequelize.DataTypes.STRING
        },
        token: {
            type: Sequelize.DataTypes.STRING
        }
        , id_firebase: {
            type: Sequelize.DataTypes.STRING
        }

    });


    chat = databse.getDatabse.define("chats", {
        idsalas: {
            type: Sequelize.DataTypes.INTEGER
        },
        datacalen: {
            type: Sequelize.DataTypes.STRING(20)
        },
        type: {
            type: Sequelize.DataTypes.CHAR(1)
        },
        horario: {
            type: Sequelize.DataTypes.STRING(10)
        }, texto: {
            type: Sequelize.DataTypes.TEXT
        }, indetifique: {
            type: Sequelize.DataTypes.INTEGER
        }
    });

    clientes.sync();
    produtos.sync();
    categoria.sync();
    device.sync();
    salas.sync();
    chat.sync();
    admin.sync();


};


/*clientes.findOne().then((user) => {


}).catch((err) => console.error(err));*/



