var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var hbs = require('hbs');
const PojoController = require('./bd/PojoController');


const ServerWS = require('./lib/ServerWS');

const index = require('./rotas/index');
const admin = require('./rotas/admin/admin');
const mobile = require('./rotas/mobile/mobile');
const up = require('./rotas/upload');


var pojo = PojoController.getInstance;


var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: true, limit: '50mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));


var blocks = {};

hbs.registerHelper('extend', function (name, context) {
    var block = blocks[name];
    if (!block) {
        block = blocks[name] = [];
    }

    block.push(context.fn(this));
});

hbs.registerHelper('block', function (name) {
    var val = (blocks[name] || []).join('\n');

    // clear the block
    blocks[name] = [];
    return val;
});


app.use('/', index);
app.use('/ad', admin);
app.use('/mobile', mobile);
app.use('/up', up);


/**
 *
 * @return {PojoController}
 */
module.exports.getdatabase = () => {
    return pojo
};


/**
 *
 * @return {ServerWS}
 */
module.exports.ws = () => {
    return ServerWS.getInstancie
};



module.exports = app;


module.exports.sev = function (server) {

    var serve = new ServerWS(server);
    serve.init();
    ServerWS.setServerWS = serve;


};