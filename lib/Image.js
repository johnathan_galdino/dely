

'use strict';


const path = require('path');
const fs = require('fs');


class Image {


    constructor(){



    }


    /**
     *
     * @param {String} pathold
     * @param {String} filename
     * @param {Function} fn
     */
     upload( pathold ,filename,fn)  {

        var pa =   path.join(__dirname, '../public/images/' + filename  + ".jpg");

        fs.rename(pathold, pa, function(error) {
            if (error) {
                fn(false);
                throw error;
            } else {
                fn(true)
            }
        });
    }



    /**
     * @param  {string} filename
     * @param res
     */
    delete(filename,res){

        fs.stat(path.join(__dirname, '../public/images/' + filename ),function (err) {

            if(err){
                return console.error(err)
            }

            fs.unlink(path.join(__dirname, '../public/images/' + filename ),async function (err) {

                if(!err){

                    var d = await {status:'ok'};
                    res.json(d)
                }


            })

        })

    }

    /**
     *
     * @return {[]}
     */
     nameFilesFolder(res){


        fs.readdir(path.join(__dirname, '../public/images/'), async (err,files)=>{

            if(!err){



                res.json(files)

            }

        });



    }


    getfile(name, res){


        fs.readdir(path.join(__dirname, '../public/images/'), async (err,files)=>{

            if(!err){

                files.forEach(value => {


                    if(name === value){

                        res.sendFile('../public/images/'+ name);
                        res.end();

                    }



                });



            }

        });



    }

}


module.exports = Image;
