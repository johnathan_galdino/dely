'use strict';


const request = require('request');


class Notification {
    get keygoogle() {
        return this._keygoogle;
    }

    set keygoogle(value) {
        this._keygoogle = value;
    }

    get res() {
        return this._res;
    }

    set res(value) {
        this._res = value;
    }


    constructor(keygoogle, res) {


        this._keygoogle = keygoogle;
        this._res = res;
    }


    /**
     *
     * @param data
     * @param {Array} array
     */
    sendAll(data, array) {

        const self = this;

        var registrationIDs = [];

        console.log("Token1" + registrationIDs.length);

        for (var i = 0, tam = array.length; i < tam; i++) {

            registrationIDs.push(array[i]);
        }

        console.log("Token2" + registrationIDs.length);

        var ta = Math.ceil(registrationIDs.length / 1000); // GCM PERMITE APENAS 1000 REGISTROS POR ENVIO

        var c;
        for (var i = c = 0; i < ta; i++) {

            var gcmIds = [];
            for (var j = 0; j < 1000 && this.issert(registrationIDs[j + c]); j++) {
                gcmIds.push(registrationIDs[j + c]);
            }
            c += 1000;


            console.log("Token" + gcmIds.length);

            request({
                url: 'https://fcm.googleapis.com/fcm/send',
                method: 'POST',
                headers: {
                    'Content-Type': ' application/json',
                    'Authorization': 'key=' + this._keygoogle
                },
                body: JSON.stringify(
                    {
                        registration_ids: gcmIds,
                        data: data,
                        notification: {
                            body: data.texto,
                            title: data.titulo,
                        },
                        //  restricted_package_ : "ead.elite.app.br.com.appelite.ead",
                        priority: "high"
                    })
            }, function (error, response, body) {
                if (error) {
                    console.error(error, response, body);


                } else if (response.statusCode >= 400) {

                    console.log(response.statusCode + ' - ' + response.statusMessage + '\n');

                    console.error('HTTP Error: ' + response.statusCode + ' - ' + response.statusMessage + '\n' + body);

                    self._res.json(response);

                    console.log(response);

                    self._res.end();

                } else {


                    console.log('Done!');
                    self._res.write(body);
                    console.log(response.statusCode + ' - ' + response.statusMessage + '\n');
                    self._res.end();

                }
            });


        }


    }


    /**
     *
     * @param data
     * @param {String} topic
     */
    sendTopic(data, topic) {


        const self = this;


        request({
            url: 'https://fcm.googleapis.com/fcm/send',
            method: 'POST',
            headers: {
                'Content-Type': ' application/json',
                'Authorization': 'key=' + this._keygoogle
            },
            body: JSON.stringify(
                {
                    to: "/topics/" + topic,
                    data: data,
                    restricted_package_: "cc.fabricaandroid.ecomence",
                    priority: "high"
                })
        }, function (error, response, body) {

            if (error) {

                console.error(error, response, body);

            } else if (response.statusCode >= 400) {


                console.log(response.statusCode + ' - ' + response.statusMessage + '\n');

                console.error('HTTP Error: ' + response.statusCode + ' - ' + response.statusMessage + '\n' + body);

                self._res.json(response);
                self._res.end();

            } else {


                console.log('Done!');
                self._res.write(body);
                console.log(response.statusCode + ' - ' + response.statusMessage + '\n');


                self._res.end();

            }
        });


    }


    /**
     *
     * @param data
     * @param {String} token
     */
    sendDevice(data, token) {


        const self = this;

        /*  var notification = {
              'title': 'Portugal vs. Denmark',
              'body': '5 to 1',
              'icon': 'firebase-logo.png',
              'click_action': 'http://localhost:8081'
          }; */

        console.log(token);

        request({
            url: 'https://fcm.googleapis.com/fcm/send',
            method: 'POST',
            headers: {
                'Content-Type': ' application/json',
                'Authorization': 'key=' + this._keygoogle
            },
            body: JSON.stringify(
                {
                    notification: {
                        body: data.texto,
                        title: data.titulo,
                    },
                    data: data,
                    to: token
                }
            )
        }, function (error, response, body) {
            if (error) {
                console.error(error, response, body);
            } else if (response.statusCode >= 400) {

                console.log(response.statusCode + ' - ' + response.statusMessage + '\n');
                self._res.json(response);
                self._res.end();
                console.error('HTTP Error: ' + response.statusCode + ' - ' + response.statusMessage + '\n' + body);
            } else {
                console.log('Done!');
                console.log(response.statusCode + ' - ' + response.statusMessage + '\n');
                self._res.write(body);
                self._res.end();

            }
        });


    }

    /**
     *
     * @param {String} str
     * @returns {boolean}
     */
    issert(str) {


        return (typeof str !== 'undefined');
    }

}


module.exports = Notification;