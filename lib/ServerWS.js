const WebSocket = require('ws').Server;


class ServerWS {

    /**
     *
     * @param {ServerWS} _setServerWS
     */
    static set setServerWS(_setServerWS){

        this._setServerWS =  _setServerWS;

        if(this._setServerWS === null){
            console.log("null")
        }

    }

    /**
     *
     * @return {ServerWS}
     */
    static get getInstancie(){

        if(this._setServerWS === null){
            console.log("null")
        }

       return this._setServerWS
    }

    /**
     *
     * @param {Server} server
     */
    constructor(server) {

        this.wss = new WebSocket({server});




        this.wss.broadcast = function broadcast(data) {
           this.wss.clients.forEach(function each(client) {
                if (client.readyState === WebSocket.OPEN) {
                    client.send(data);
                }
            });
        };

    }


    send(data) {
        this.wss.clients.forEach(function each(client) {
            client.send(data);

        });

    }



    init(){

      this.wss.on('connection', function connection(ws) {

            ws.on('message', function incoming(message) {


            });

        });

    }


}


module.exports = ServerWS;