var express = require('express');
var router = express.Router();
const Image = require('../../lib/Image');
const app = require('../../app');
const request = require('request');

router.post('/', function(req, res, next) {



    var dados = req.body;

    var pojo = app.getdatabase();



    // CATEGORIA
    if (dados.method === 'categorias') {

        pojo.getCategoria.findAll({where:{ativado:true}}).then((users) => {

            res.json(users);
           

        }).catch((err) => {
            console.log(err)

        });


    }

    //CLIENTES
    if (dados.method === 'cliente') {


        pojo.getClientes.findOne({where:{acerro:'true',idfirebase:dados.idfirebase}}).then((users) => {

            res.json(users);

        }).catch((err) => {
            console.log(err)

        });


    }


    if (dados.method === 'isertcliente') {


        pojo.getClientes.update({
            nome:dados.nome,
            endereco: dados.endereco,
            nu:dados.nu,
            cidade:dados.cidade,
            telefone:dados.telefone,
            email:dados.email,
            senha:"",
            acesso:"true",
            idfirebase: dados.idfirebase,
            typecli: 1,
            }).then(()=>{

            var d = {status: 'ok'};
            res.json(d)

        }).catch((err)=>{})


    }

	//PRODUTOS
	if (dados.method === 'getprodutos') {


        pojo.getProduto.findAll({where:{idcategoria:dados.id}}).then((users) => {

            res.json(users);

        }).catch((err) => {
            console.log(err)

        });


    }





    if (dados.method === 'insertpedido') {


       // app.ws().send('insertpedido')


    }


    if(dados.method === 'send'){


        var key = 'AIzaSyANjpb92QKKnmTthWCutcVmmsFFZtCNU2E';

        var to = 'dAl6__6XifE:APA91bF2_7Rj2TervGnlFLmwNKKKGsdsdV9Hdsjs1mKXS7dbzkuqZydSdox2vHiTuXWZGk0YRmPX7JdnkDpSOj1rWt1h92cknH2Su8mZVkcgWApO_6Vu7pELZrEekZ_uiiwXrTXamclw';

        var notification = {
            'title': 'Portugal vs. Denmark',
            'body': '5 to 1',
            'icon': 'firebase-logo.png',
            'click_action': 'http://localhost:8081'
        };


        request({
            url: 'https://fcm.googleapis.com/fcm/send',
            method: 'POST',
            headers: {
                'Content-Type' :' application/json',
                'Authorization': 'key='+ key
            },
            body: JSON.stringify(
                { "data": {
                        "message": notification
                    },
                    "to" : to
                }
            )
        }, function(error, response, body) {
            if (error) {
                console.error(error, response, body);
            }
            else if (response.statusCode >= 400) {

                res.write(response.statusCode+' - '+response.statusMessage+'\n'+body);
                res.end();
                console.error('HTTP Error: '+response.statusCode+' - '+response.statusMessage+'\n'+body);
            }
            else {
                console.log('Done!');

                res.write(response.statusCode+' - '+response.statusMessage+'\n'+body);
                res.end();
            }
        });



    }


    //MOBILE
    if(dados.method === 'uptoken'){


        pojo.getClientes.findOne({where:{idfirebase:dados.id}}).then((users) => {

            pojo.getDevices.update({token: dados.token}, {where: {idclientes: users.id}}).then(() => {
                var d = {status: 'ok'};
                res.json(d)
            }).catch((err) => {
                console.log(err)
            })

        }).catch((err) => {
            console.log(err)

        });



        

    }

});


module.exports = router;