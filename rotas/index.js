var express = require('express');
var router = express.Router();
const Image = require('../lib/Image');
const Notification = require("../lib/notification");
const app = require('../app');
var fs = require('fs');
var path = require('path');


router.get('/', function (req, res, next) {

    fs.readFile('config.json', 'utf8', (err, responce) => {

        var obj = JSON.parse(responce);
        res.render('index', {title: "Login", nomeempresa: obj.app.nome});
    });


});


router.get('/images', function (req, res, next) {

    var img = req.query.id;

    res.sendFile(path.join(__dirname, "../public/images/" + img));


});


router.post('/', function (req, res, next) {


    var dados = req.body;

    //  var dados = req.body;


    var pojo = app.getdatabase();


    //IMAGE REQUEST
    if (dados.method === 'getimage') {

        var image = new Image();

        image.nameFilesFolder(res);

    }
    if (dados.method === 'deleteimage') {
        var image = new Image();

        image.delete(dados.id, res);

    }
    //CATEGORIA REQUEST
    if (dados.method === 'gecategorias') {


        pojo.getCategoria.findAll().then((users) => {

            res.json(users);

        }).catch((err) => {
            console.log(err)
        });


    }
    if (dados.method === 'insertcategoria') {


        pojo.getCategoria.create({nome: dados.nome, itcadas: 0, ativado: false})
            .then(() => {

                var d = {status: 'ok'};
                res.json(d)

            }).catch((err) => {
            console.log(err)
        });


    }
    if (dados.method === 'excluircate') {


        pojo.getCategoria.destroy({where: {id: dados.id}})
            .then(() => {

                var d = {status: 'ok'};
                res.json(d)

            }).catch((err) => {
            console.log(err)
        });
    }
    if (dados.method === 'upcheckcate') {

        pojo.getCategoria.update({ativado: dados.check}, {where: {id: dados.id}}).then(() => {
            var d = {status: 'ok'};
            res.json(d)
        }).catch((err) => {
            console.log(err)
        })

    }
    //PRODUTOS REQUEST
    if (dados.method === 'insertProdut') {


        pojo.getProduto.create({
            nome: dados.nome,
            precoc: dados.precoc,
            precov: dados.precov,
            quanti: dados.quatidade,
            idcategoria: dados.categoria,
            fotoname1: dados.fotoname1,
            fotoname2: dados.fotoname2,
            fotoname3: dados.fotoname3,
            fotoname4: dados.fotoname4,
            descricao: dados.drecrisao,
            tipomedida: dados.medida,
            valormedida: dados.vmedida,
            abilitpro: dados.abilitapro,
            valorpro: dados.valorpromo !== "" ? dados.valorpromo : "0",
            ocuta: dados.ocuta
        }).then(() => {


            var d = {status: 'ok'};
            res.json(d)


        }).catch((err) => {

            console.log(err)

        })

    }
    if (dados.method === 'getProduts') {


        var value = [];

        pojo.getProduto.findAll().then(async (users) => {


            for (var i = 0; i < Object.keys(users).length; i++) {

                var user = users[i];

                var va = await pojo.getCategoria.findOne({where: {id: user.idcategoria}});


                value.push({produto: user, cate: va});

            }

            res.json(value);


        }).catch((err) => {
            console.log(err)
        });

    }
    if (dados.method === 'deleteProduts') {

        pojo.getProduto.destroy({where: {id: dados.id}})
            .then(() => {

                var d = {status: 'ok'};
                res.json(d)

            }).catch((err) => {
            console.log(err)
        });

    }
    if (dados.method === 'upProduts') {

        pojo.getProduto.update({
                nome: dados.nome,
                precoc: dados.precoc,
                precov: dados.precov,
                quanti: dados.quatidade,
                idcategoria: dados.categoria,
                fotoname1: dados.fotoname1,
                fotoname2: dados.fotoname2,
                fotoname3: dados.fotoname3,
                fotoname4: dados.fotoname4,
                descricao: dados.drecrisao,
                tipomedida: dados.medida,
                valormedida: dados.vmedida,
                abilitpro: dados.valorpromo !== "" ? dados.valorpromo : "0",
                valorpro: dados.valorpromo
            }
            , {where: {id: dados.id}}).then(() => {


            var d = {status: 'ok'};
            res.json(d)


        }).catch((err) => {

            console.log(err)

        })
    }

        //TOKEN
    if (dados.method === 'uptokenadmin') {

        pojo.getAdmin.update({token: dados.token}, {where: {id_firebase: dados.id}}).then(() => {
            var d = {status: 'ok'};
            res.json(d)
        }).catch((err) => {
            console.log(err)
        })

    }

    /* mobile */
    if (dados.method === 'devices') {

        pojo.getDevices.create({token: dados.token, idclientes: dados.uid}).then(() => {


            var d = {status: 'ok'};
            res.json(d)


        }).catch((err) => {

            console.log(err)

        })

    }


//MENSSAGE
    if (dados.method === 'sendnoti') {


        var notification = new Notification('AAAAcEttH3o:APA91bHIBzQeE_pyc81r3fcrh0KytZ9q8aVwGStcG_UMLrcD4csBY_czjAt0OxIkBajQFityI4KhGFVg5Z4PVLOOWAn_wZIcKk5H9H5EI5_Vg-t4zZOF_SELIMUjFVQqmyne73um4MOq', res);


        if (dados.token !== "null") {

            pojo.getDevices.findOne({where: {idclientes: dados.token}}).then((user) => {

                notification.sendDevice(dados, user.token);


            }).catch((err) => {

                console.log(err)

            })

        } else {


            pojo.getDevices.findAll().then((user) => {

                var token = [];

                user.forEach((us) => {


                    token.push(us.token);

                });

                notification.sendAll(dados, token);


            }).catch((err) => {

                console.log(err)

            })


        }


    }


    if (dados.method === 'savemessage') {


        var data = JSON.parse(dados.dado);


        console.log(data);

        //CHAT
        if (data.type === "c") {



            if (data.token !== "null") {



                pojo.getSalas.findOne({where: {idclient: data.token}}).then( (sala) => {

                    console.log(sala);

                    if (sala !== null) {

                          pojo.getClientes.findOne({where: {id: sala.idclient}}).then((array)=>{


                              if(array === null){

                                  pojo.getSalas.create({
                                      idclient: data.token,
                                      endereco: "",
                                      naol: 0

                                  }).then((value) => {


                                      pojo.getChat.create({
                                          idsalas: value.id,
                                          datacalen: "",
                                          type: data.type,
                                          texto: data.texto,
                                          indetifique:1
                                      }).then((value) => {


                                          var d = {status: 'ok', token: value.idclient};
                                          res.json(d)


                                      }).catch((er) => {

                                          console.log(er);
                                      });


                                  }).catch((err) => {

                                      console.log(err);

                                  });

                              } else {


                                    pojo.getChat.create({
                                        idsalas: sala.id,
                                        datacalen: "",
                                        type: data.type,
                                        texto: data.texto,
                                        indetifique: 1
                                    }).then((value) => {

                                        res.json(value)


                                    }).catch((er) => {

                                        console.log(er);
                                    });


                                }





                        }).catch((err)=>{});



                    } else {

                        console.log("null sala");

                        pojo.getClientes.findOne({where: {id: data.token}}).then((ar) => {


                           if(ar !== null){


                               pojo.getSalas.create({
                                   idclient: ar.id,
                                   endereco: ar.endereco,
                                   naol: 0

                               }).then((value) => {


                                   pojo.getChat.create({
                                       idsalas: value.id,
                                       datacalen: "",
                                       type: data.type,
                                       texto: data.texto,
                                       indetifique: 1
                                   }).then(() => {


                                       var d = {status: 'ok', token: value.idclient};
                                       res.json(d)

                                   }).catch((er) => {

                                       console.log(er);
                                   });


                               }).catch((err) => {

                                   console.log(err);

                               });

                           }





                        }).catch((err) => {


                        });


                    }


                }).catch((err) => {
                })
            }

        } else {


        }


    }


    //CLIENTES REQUEST
    if (dados.method === 'getclinet') {

        pojo.getClientes.findAll().then((users) => {

            res.json(users);

        }).catch((err) => {
            console.log(err)
        });

    }

    if (dados.method === 'getclinetid') {

        pojo.getClientes.findAll({where: {id: dados.data}}).then((users) => {

            res.json(users);

        }).catch((err) => {
            console.log(err)
        });

    }


    //SALAS
    if (dados.method === 'getclinetsalas') {

        pojo.getSalas.findAll().then((salas)=>{


            if(salas !== null){

                res.json(salas);
            }


        }).catch((err)=>{});

    }


    //CHAT
    if (dados.method === 'chat') {

        pojo.getChat.findAll({where: {idsalas: dados.data,type:'c'}}).then((array)=>{

            res.json(array)


        }).catch((err)=>{})
    }

    if (dados.method === 'sendmensagen') {


        pojo.getSalas.findOne({where: { id: dados.idclisala}}).then((sala)=>{

            if(sala !== null){

                pojo.getDevices.findOne({where: { idclientes: sala.idclient}}).then((user) => {

                    if(user !== null){

                        pojo.getChat.create({
                            idsalas: dados.idclisala,
                            datacalen: "",
                            type: dados.type,
                            texto: dados.text,
                            indetifique: dados.indetifique
                        }).then(() => {

                            var d = {status: 'ok', token: sala.idclient};
                            res.json(d)

                        }).catch((er) => {

                            console.log(er);
                        });

                    }


                }).catch((err)=>{});



            }


        }).catch((err)=>{});






    }


    //DEVICES
    if(dados.method === "devicescliente"){


        devicescli(pojo).then(value1 =>  res.json(value1));

    }


});



async function devicescli(pojo) {

    var devices = await pojo.getDevices.findAll();

    var clientes = await  pojo.getClientes.findAll();


    return  {status:"ok" , devices:devices, clientes: clientes };

}



module.exports = router;